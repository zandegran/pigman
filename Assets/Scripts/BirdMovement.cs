﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
public class BirdMovement : MonoBehaviour {
	public List<Vector2> path1;
	public List<Vector2> path2;
	public List<Vector2> path3;
	public List<Vector2> path4;
	private bool powerMode =false;
	private float timeLeft = 5.0f;
	private Vector3 refScale=Vector3.zero;
	int cur = 0;
	public float speed = 0.3f;
	// Use this for initialization
	void Start () {
		initializePaths ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//print(transform.position+" to "+path1[cur]+" "+(transform.position == (Vector3)path1[cur]));
		List<Vector2> path=getPath();
		if (transform.position != (Vector3)path [cur]) {
			Vector2 p = Vector2.MoveTowards (transform.position,
				            path [cur],
							powerMode?speed*.6f:speed);
			GetComponent<Rigidbody2D> ().MovePosition (p);
			rotate (path [cur] - (Vector2)transform.position);

		} else {
			if (!powerMode)
				cur = (cur + 1) % path.Count;
			else
				cur = (cur==0?path.Count-1:cur -1);
		}
		timeLeft -= Time.deltaTime;
		if(timeLeft < 0)
		{
			powerMode = false;
			GetComponent<Animator>().SetBool("Flash", false);
		}
	}

	void OnTriggerEnter2D(Collider2D collided) {
		//print (collided.name);

		if (collided.name == "Piggie" ) {
			if (!powerMode) {
				removeLife (collided);
				takePiggieHome (collided.gameObject);
			}


		}
	}
	public void setPowerMode(){
		powerMode =true;
		print (powerMode);
		timeLeft = 5.0f;
		GetComponent<Animator>().SetBool("Flash", true);
		//iTween.ShakeRotation (gameObject, new Vector3 (30, 30, 0), 5.0f);
	}
	void takePiggieHome(GameObject piggie){
		Vector3 scale = piggie.transform.localScale;
		iTween.ScaleTo(piggie, iTween.Hash("x",0,"y",0,"z",0, "easeType", "easeOutElastic", "loopType", "none", "time",.1));
		Movement m=piggie.GetComponent<Movement> ();
		m.goHome ();
		iTween.ScaleTo(piggie, iTween.Hash("x",scale.x,"y",scale.y,"z",scale.z, "easeType", "easeOutElastic", "loopType", "none", "time",0.7,"delay",.12));
	}
	void removeLife(Collider2D collided)
	{
		GameObject Lives = GameObject.Find ("Lives");
		//RemoveLife removeLife= Lives.GetComponent<RemoveLife>();
		//removeLife.remove ();
		foreach (Transform child in Lives.transform) {
			Rigidbody2D rb = child.GetComponent<Rigidbody2D> ();
			if(rb!=null)
				rb.gravityScale = 1;
			GameObject go = child.gameObject;
			Destroy (go);
			break;
		}
		if (Lives.transform.childCount==1)Destroy (collided.gameObject);
	}
	void rotate(Vector2 direction)
	{
		int angle = 0;
		if(direction.x<0) angle = 270;
		if(direction.x>0) angle = 90;
		if(direction.y<0) angle = 0;
		if(direction.y>0) angle = 180;
		float time = 0.1f + (speed - 1) * (0.7f - 0.1f) / (0.1f - 1); //Linear interpulation
		iTween.RotateTo(gameObject, iTween.Hash("z",angle, "easeType", "easeOutElastic", "loopType", "none", "time",time));
	}
	List<Vector2> getPath(){
		if(gameObject.name=="Birdie1")return  path1;
		if(gameObject.name=="Birdie2")return  path2;
		if(gameObject.name=="Birdie3")return  path3;
		if(gameObject.name=="Birdie4")return  path4;
		return  path2;
	}

	void initializePaths(){
		path1.Add (new Vector2(-1,2));
		path1.Add (new Vector2(-4,2));
		path1.Add (new Vector2(-4,-2));
		path1.Add (new Vector2(-4,-5));
		path1.Add (new Vector2(-6,-5));
		path1.Add (new Vector2(-6,-2));
		path1.Add (new Vector2(-8,-2));
		path1.Add (new Vector2(-8,-0));
		path1.Add (new Vector2(-13,-0));
		path1.Add (new Vector2(-13,-11));
		path1.Add (new Vector2(-9,-11));
		path1.Add (new Vector2(-9,-7));
		path1.Add (new Vector2(-1,-7));
		path1.Add (new Vector2(-1,-5));
		path1.Add (new Vector2(4,-5));
		path1.Add (new Vector2(4,2));

		path2.Add (new Vector2(1,2));
		path2.Add (new Vector2(11,2));
		path2.Add (new Vector2(11,0));
		path2.Add (new Vector2(8,0));
		path2.Add (new Vector2(8,-2));
		path2.Add (new Vector2(6,-2));
		path2.Add (new Vector2(6,-4));
		path2.Add (new Vector2(10,-4));
		path2.Add (new Vector2(10,-5));
		path2.Add (new Vector2(13,-5));
		path2.Add (new Vector2(13,-7));
		path2.Add (new Vector2(9,-7));
		path2.Add (new Vector2(9,-11));
		path2.Add (new Vector2(1,-11));
		path2.Add (new Vector2(1,-9));
		path2.Add (new Vector2(3,-9));
		path2.Add (new Vector2(3,-7));
		path2.Add (new Vector2(1,-7));
		path2.Add (new Vector2(1,-5));
		path2.Add (new Vector2(-4,-5));
		path2.Add (new Vector2(-4,-3));
		path2.Add (new Vector2(4,-3));
		path2.Add (new Vector2(4,2));

		path3.Add (new Vector2(-1,4));
		path3.Add (new Vector2(-3,4));
		path3.Add (new Vector2(-3,6));
		path3.Add (new Vector2(-5,6));
		path3.Add (new Vector2(-5,2));
		path3.Add (new Vector2(-9,2));
		path3.Add (new Vector2(-9,4));
		path3.Add (new Vector2(-13,4));
		path3.Add (new Vector2(-13,11));
		path3.Add (new Vector2(-1,11));
		path3.Add (new Vector2(-1,9));
		path3.Add (new Vector2(1,9));
		path3.Add (new Vector2(1,6));
		path3.Add (new Vector2(3,6));
		path3.Add (new Vector2(3,4));
		path3.Add (new Vector2(1,4));
		path3.Add (new Vector2(1,2));
		path3.Add (new Vector2(-1,2));

		path4.Add (new Vector2(1,2));
		path4.Add (new Vector2(5,2));
		path4.Add (new Vector2(5,6));
		path4.Add (new Vector2(1,6));
		path4.Add (new Vector2(1,11));
		path4.Add (new Vector2(13,11));
		path4.Add (new Vector2(13,4));
		path4.Add (new Vector2(7,4));
		path4.Add (new Vector2(7,6));
		path4.Add (new Vector2(5,6));
		path4.Add (new Vector2(5,2));
	}



}
