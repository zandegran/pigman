﻿using UnityEngine;
using System.Collections;
using System;

public class OrganizeMaze : MonoBehaviour {

	// Puts Maze blocks in exact spots. 
	//Doing this to save time and I think it's a bad Idea
	void Start () {
		foreach (Transform child in transform)
		{
			child.transform.position = new Vector2 ((float)Math.Round((float)child.transform.position.x, 0), (float)Math.Round((float)child.transform.position.y, 0));
		}
	}

}
