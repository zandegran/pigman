﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class FillPoints : MonoBehaviour {
	SpriteRenderer sprite;
	CircleCollider2D collider;
	public List<Vector2> noFillArea;
	// Use this for initialization
	void Start () {
		initializeArray ();
		CheckAndPutPoint ();
	
	}


	private void CheckAndPutPoint()
	{
		// Get the current sprite with an unscaled size
		sprite = GetComponent<SpriteRenderer>();
		collider = GetComponent<CircleCollider2D>();
		// Generate a child prefab of the sprite renderer
		GameObject childPrefab = new GameObject();
		SpriteRenderer childSprite = childPrefab.AddComponent<SpriteRenderer>();
		CircleCollider2D childCollider=childPrefab.AddComponent<CircleCollider2D>();
		childPrefab.transform.position = transform.position;//new Vector2(x,y);
		childPrefab.transform.localScale = transform.localScale;
		childPrefab.transform.parent = transform.parent;
		childPrefab.AddComponent<point>();
		childSprite.sprite = sprite.sprite;
		UnityEditorInternal.ComponentUtility.CopyComponent (collider);
		UnityEditorInternal.ComponentUtility.PasteComponentValues (childPrefab.GetComponent<CircleCollider2D>());
		UnityEditorInternal.ComponentUtility.CopyComponent (sprite);
		UnityEditorInternal.ComponentUtility.PasteComponentValues (childPrefab.GetComponent<SpriteRenderer>());
		for(int i=-14;i<14;i++)
			for(int j=-12;j<12;j++)
			{
				Vector2 pos = new Vector3 (i, j);
				float radius = 0.12f;

				if (Physics2D.OverlapCircle (pos, radius)) {
					// found something
					//print("Hit at :"+pos);
				} else {
					//Empty spot
					if (!isNoFill(pos))
						PutPoint (pos,childPrefab);
				}
			}


		Destroy(gameObject);
	}
	bool isNoFill(Vector2 coord){
		foreach (Vector2 vec in noFillArea) {
			if (coord == vec)
				return true;
		}
		return false;
	}
	
	void PutPoint (Vector2 coord,GameObject childPrefab) {

		GameObject child;
		child = Instantiate(childPrefab) as GameObject;
		child.transform.position = coord;//new Vector2 (x, y);
		child.transform.parent = transform.parent;
		child.transform.localScale = transform.localScale;
		// Set the parent last on the prefab to prevent transform displacement
		childPrefab.transform.parent = transform;
		// Disable the currently existing sprite component since its now a repeated image
		sprite.enabled = false;
	}


	void initializeArray(){
		/*//Stars
		noFillArea.Add (new Vector2(-13,-11));
		noFillArea.Add (new Vector2(13,-11));
		noFillArea.Add (new Vector2(-13,0));
		noFillArea.Add (new Vector2(13,0));
		noFillArea.Add (new Vector2(-1,6));
		noFillArea.Add (new Vector2(1,6));
		noFillArea.Add (new Vector2(13,11));
		noFillArea.Add (new Vector2(-13,11));*/

		//Spawning Area
		noFillArea.Add (new Vector2(-2,0));
		noFillArea.Add (new Vector2(-1,0));
		noFillArea.Add (new Vector2(0,0));
		noFillArea.Add (new Vector2(1,0));
		noFillArea.Add (new Vector2(2,0));
		noFillArea.Add (new Vector2(-2,-1));
		noFillArea.Add (new Vector2(-1,-1));
		noFillArea.Add (new Vector2(0,-1));
		noFillArea.Add (new Vector2(1,-1));
		noFillArea.Add (new Vector2(2,-1));
		noFillArea.Add (new Vector2(-1,1));
		noFillArea.Add (new Vector2(0,1));
		noFillArea.Add (new Vector2(1,1));
		/*
		//Boxes (Closed Areas)
		noFillArea.Add (new Vector2(-11,-9));
		noFillArea.Add (new Vector2(-7,-9));
		noFillArea.Add (new Vector2(-6,-9));
		noFillArea.Add (new Vector2(-5,-9));
		noFillArea.Add (new Vector2(11,-9));
		noFillArea.Add (new Vector2(7,-9));
		noFillArea.Add (new Vector2(6,-9));
		noFillArea.Add (new Vector2(5,-9));

		noFillArea.Add (new Vector2(-11,7));
		noFillArea.Add (new Vector2(-10,7));
		noFillArea.Add (new Vector2(-9,7));
		noFillArea.Add (new Vector2(-11,6));
		noFillArea.Add (new Vector2(-10,6));
		noFillArea.Add (new Vector2(-9,6));
		noFillArea.Add (new Vector2(11,7));
		noFillArea.Add (new Vector2(10,7));
		noFillArea.Add (new Vector2(9,7));
		noFillArea.Add (new Vector2(11,6));
		noFillArea.Add (new Vector2(10,6));
		noFillArea.Add (new Vector2(9,6));

		noFillArea.Add (new Vector2(-5,9));
		noFillArea.Add (new Vector2(-4,9));
		noFillArea.Add (new Vector2(-3,9));
		noFillArea.Add (new Vector2(-5,8));
		noFillArea.Add (new Vector2(-4,8));
		noFillArea.Add (new Vector2(-3,8));
		noFillArea.Add (new Vector2(5,9));
		noFillArea.Add (new Vector2(4,9));
		noFillArea.Add (new Vector2(3,9));
		noFillArea.Add (new Vector2(5,8));
		noFillArea.Add (new Vector2(4,8));
		noFillArea.Add (new Vector2(3,8));
		*/
	}

}

