﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class FillStars : MonoBehaviour {
	SpriteRenderer sprite;
	CircleCollider2D collider;
	public List<Vector2> starPositions;

	// Use this for initialization
	void Start () {
		initializeArray ();
		CheckAndPutStar ();

	}
	void initializeArray(){
		starPositions.Add (new Vector2(-13,-11));
		starPositions.Add (new Vector2(13,-11));
		starPositions.Add (new Vector2(-13,0));
		starPositions.Add (new Vector2(13,0));
		starPositions.Add (new Vector2(-1,6));
		starPositions.Add (new Vector2(1,6));
		starPositions.Add (new Vector2(13,11));
		starPositions.Add (new Vector2(-13,11));
	}
	private void CheckAndPutStar()
	{
		// Get the current sprite with an unscaled size
		sprite = GetComponent<SpriteRenderer>();
		collider = GetComponent<CircleCollider2D>();
		// Generate a child prefab of the sprite renderer
		GameObject childPrefab = new GameObject();
		SpriteRenderer childSprite = childPrefab.AddComponent<SpriteRenderer>();
		CircleCollider2D childCollider=childPrefab.AddComponent<CircleCollider2D>();
		childPrefab.transform.position = transform.position;//new Vector2(x,y);
		childPrefab.transform.localScale = transform.localScale;
		childPrefab.transform.parent = transform.parent;
		childPrefab.AddComponent<star>();
		childSprite.sprite = sprite.sprite;
		UnityEditorInternal.ComponentUtility.CopyComponent (collider);
		UnityEditorInternal.ComponentUtility.PasteComponentValues (childPrefab.GetComponent<CircleCollider2D>());
		UnityEditorInternal.ComponentUtility.CopyComponent (sprite);
		UnityEditorInternal.ComponentUtility.PasteComponentValues (childPrefab.GetComponent<SpriteRenderer>());
		foreach (Vector2 vec in starPositions) {

			PutStar (vec,childPrefab);
		}

		Destroy(gameObject);
			
	}






void PutStar (Vector2 vec,GameObject childPrefab) {

		GameObject child;
		child = Instantiate(childPrefab) as GameObject;
		child.transform.position = vec;
		child.transform.parent = transform.parent;
		child.transform.localScale = transform.localScale;
		// Set the parent last on the prefab to prevent transform displacement
		childPrefab.transform.parent = transform;
		// Disable the currently existing sprite component since its now a repeated image
		sprite.enabled = false;
	}
}
