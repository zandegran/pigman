﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ScoreKeeper : MonoBehaviour {
	public static int score=0;
	[SerializeField]
	private Text Score;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Score.text = "Score\n" + score;
	}
}
