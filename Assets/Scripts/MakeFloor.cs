﻿using UnityEngine;
using System.Collections;

public class MakeFloor : MonoBehaviour {
	SpriteRenderer sprite;
	// Use this for initialization
	void Start () {
		make ();
	}



	void make () {
		
		// Get the current sprite with an unscaled size
		sprite = GetComponent<SpriteRenderer>();
		Vector2 spriteSize = new Vector2(sprite.bounds.size.x / transform.localScale.x, sprite.bounds.size.y / transform.localScale.y);

		// Generate a child prefab of the sprite renderer
		GameObject childPrefab = new GameObject();
		SpriteRenderer childSprite = childPrefab.AddComponent<SpriteRenderer>();
		childPrefab.transform.position = transform.position;//new Vector2(x,y);
		childPrefab.transform.localScale = transform.localScale;
		childSprite.sprite = sprite.sprite;


		GameObject child;
		for(int i=-14;i<14;i++)
			for(int j=-12;j<12;j++)
			{
				child = Instantiate(childPrefab) as GameObject;
				child.transform.position = new Vector2 (i, j);
				child.transform.parent = transform;

				
			}


		// Set the parent last on the prefab to prevent transform displacement
		childPrefab.transform.parent = transform;

		// Disable the currently existing sprite component since its now a repeated image
		sprite.enabled = false;
	}

}
