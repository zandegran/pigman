﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {
	public float speed = 0.02f;
	Vector2 _dest = Vector2.zero;
	Vector2 _dir = Vector2.zero;
	Vector2 _nextDir = Vector2.zero;
	private int angle=0;
	// Use this for initialization
	void Start () {
		_dest = transform.position;
	}

	// Update is called once per frame
	void FixedUpdate () {
		Move ();
	}

	bool canMove(Vector2 direction)
	{
		Vector2 pos = transform.position;
		direction += new Vector2(direction.x * 0.4f, direction.y * 0.4f);
		RaycastHit2D hit = Physics2D.Linecast(pos + direction, pos);
		//print (hit.collider.name);
		return (hit.collider == GetComponent<Collider2D> ())|| hit.collider.name == "New Game Object(Clone)" ;
	}
	void Move()
	{
		Vector2 p = Vector2.MoveTowards(transform.position, _dest, speed);
		GetComponent<Rigidbody2D>().MovePosition(p);

		//Keystrokes
		if (Input.GetAxis("Horizontal") > 0) {
			_nextDir = Vector2.right;
			angle = 90;
		}
		if (Input.GetAxis ("Horizontal") < 0) {
			_nextDir = -Vector2.right;
			angle = 270;
		}
		if (Input.GetAxis ("Vertical") > 0) {
			_nextDir = Vector2.up;
			angle = 180;
		}
		if (Input.GetAxis ("Vertical") < 0) {
			_nextDir = -Vector2.up;
			angle = 0;
		}
		// print ("H: "+Input.GetAxis ("Horizontal")+"   V: "+Input.GetAxis ("Vertical"));
		if (Vector2.Distance(_dest, transform.position) < 0.00001f)
		{
			if (canMove(_nextDir))
			{
				_dest = (Vector2)transform.position + _nextDir;
				_dir = _nextDir;
				//print ("Rotating "+angle);
				float time = 0.1f + (speed - 1) * (0.7f - 0.1f) / (0.1f - 1); //Linear interpulation
				iTween.RotateTo(gameObject, iTween.Hash("z",angle, "easeType", "easeOutElastic", "loopType", "none", "time",time));
			}
			else   // if next direction is not valid
			{
				if (canMove(_dir))  
					_dest = (Vector2)transform.position + _dir;   // continue on that direction

			}
		}
	}

	public void goHome()
	{
		transform.position = new Vector2 (0, -3);
		_nextDir = Vector2.zero;
		_dest = new Vector2(0, -3);
	}

}
